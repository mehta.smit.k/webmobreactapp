import { purchased_services } from "./data";
import Card from '@material-ui/core/Card';
import { makeStyles } from '@material-ui/core/styles';
import CardContent from '@material-ui/core/CardContent';
import Typography from '@material-ui/core/Typography';
import SubControlCard from "./SubService";
const useStyles = makeStyles({
  root: {
    minWidth: 275,
    margin: "20px",
    padding: "20px",

  },
  title: {
    fontSize: 14,
  },
});
function App() {
  const classes = useStyles();
  return (

    <Card className={classes.root}>

      <CardContent>
       
        <Typography variant="h5" component="h2">
        PURCHASED SERVICES
        </Typography>
        <Typography className={classes.title} color="textSecondary" gutterBottom>
        Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s.

        </Typography>
        {purchased_services.map(ps =>
          <div>
            <Typography variant="h5" component="h3">
            {ps.name} :
            </Typography>
            {
              ps.purchased_office_template.purchased_office_services.map(pos =>
                <div>
                  <SubControlCard imae={pos.image} name={pos.name} desc={pos.description} price={pos.price}/>
                </div>
              )
            }
          </div>
        )
        }
      </CardContent>
    </Card>
  );
}

export default App;
